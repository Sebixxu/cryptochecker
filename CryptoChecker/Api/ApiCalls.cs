﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Web;
using CryptoChecker.JsonTypes;
using CryptoChecker.Types;
using Newtonsoft.Json;

namespace CryptoChecker.Api
{
    public static class ApiCalls //: IConfiguration
    {
        private static string API_KEY = "c9411431-f2cc-4de6-8441-874b8aa956d0";

        public static string GetCoinsInfo(Configuration configuration)
        {
            var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest");

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["id"] = ParseString(GetCoinsIds(configuration));


            var test = GetCoinsIds(configuration);

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", API_KEY);
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());
            //Async?
        }


        public static string GetRealCurrencyInfo(string table, string currency)
        {
            var URL = new UriBuilder($"http://api.nbp.pl/api/exchangerates/rates/{table}/{currency}");

            var queryString = HttpUtility.ParseQueryString(string.Empty);

            URL.Query = queryString.ToString();

            var client = new WebClient();
            
            client.Headers.Add("Accepts", "application/json");
            return client.DownloadString(URL.ToString());
        }

        private static List<string> GetCoinsIds(Configuration configuration)
        {
            var URL = new UriBuilder("https://pro-api.coinmarketcap.com/v1/cryptocurrency/map");

            var queryString = HttpUtility.ParseQueryString(string.Empty);
            queryString["symbol"] = ParseString(configuration.Symbols); ;

            URL.Query = queryString.ToString();

            var client = new WebClient();
            client.Headers.Add("X-CMC_PRO_API_KEY", API_KEY);
            client.Headers.Add("Accepts", "application/json");

            var jsonCoinsIds = client.DownloadString(URL.ToString());
            return CoinsId.FromJson(jsonCoinsIds).Data.Select(x => x.Id.ToString()).ToList();
        }

        private static string ParseString(List<string> stringList) //TODO ew wyjebac bo aktualnie raczej bez sensu
        {
            string symbols = String.Join(",", stringList);

            return symbols;
        }
    }
}
