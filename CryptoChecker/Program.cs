﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using CryptoChecker.Api;
using CryptoChecker.JsonTypes;
using CryptoChecker.Printers;
using CryptoChecker.Types;
using Newtonsoft.Json;

namespace CryptoChecker
{

    class Program
    {
        public static Configuration Configuration { get; private set; }

        static void Main(string[] args)
        {
            bool reload = false;
            int reloadCount = 0;

            LoadConfiguration();

            while (true)
            {
                var cryptoCurrenciesInfo = LoadCryptoCurrencyInformation();
                var usdCurrencyInfo = LoadRealCurrency("A", "USD");

                if (reload || reloadCount == 0)
                {
                    Printer.PrintCryptoCurrenciesInformation(cryptoCurrenciesInfo, Configuration, usdCurrencyInfo);
                    reloadCount++;
                }

                var readedKey = Console.ReadKey();

                if (readedKey.Modifiers == ConsoleModifiers.Control && readedKey.Key == ConsoleKey.P)
                    reload = true;
                else if(readedKey.Modifiers == ConsoleModifiers.Control && readedKey.Key == ConsoleKey.Q)
                    Environment.Exit(0);
            }
        }

        private static void LoadConfiguration()
        {
            try
            {
                Printer.PrintConfigurationStartupLog();
                Configuration = new Configuration();
                Printer.PrintConfigurationFinalLog(Configuration.Symbols.Count);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        private static Dictionary<string, Data> LoadCryptoCurrencyInformation()
        {
            var jsonCoinsInfo = ApiCalls.GetCoinsInfo(Configuration);

            var cryptoCurrency = CryptoCurrency.FromJson(jsonCoinsInfo);

            if (cryptoCurrency.Status.ErrorMessage != null)
                throw new Exception(); //TODO Wyjątek customowy i/lub retry try 

            return cryptoCurrency.Data;
        }

        private static RealCurrency LoadRealCurrency(string table, string currency)
        {
            var usdToPlnInfo = ApiCalls.GetRealCurrencyInfo(table, currency);

            var realCurrency = RealCurrency.FromJson(usdToPlnInfo);

            return realCurrency;
        }
    }
}
