﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CryptoChecker.Attributes;
using CryptoChecker.Calculations;
using CryptoChecker.JsonTypes;
using CryptoChecker.Types;

namespace CryptoChecker.Printers
{
    public static class Printer
    {
        private const ConsoleColor TextLogColor = ConsoleColor.DarkMagenta;

        private const ConsoleColor Test = ConsoleColor.Cyan;

        private const ConsoleColor Test2 = ConsoleColor.DarkRed;

        //TODO pomyśleć o nadaniu atrybtu bo tutaj tylko string będzie różny kolor moze byc przekazywany ale czyszczenie zawsze jest takie samo

        [PrinterLogAttributes(TextLogColor)]
        public static void PrintConfigurationStartupLog()
        {
            Console.WriteLine("Loading your config file.");
        }

        [PrinterLogAttributes(TextLogColor)]
        public static void PrintConfigurationFinalLog(int cryptoCount)
        {
            Console.WriteLine($"Loading was successful. Loaded your {cryptoCount} crypto currencies.");
        }

        public static void PrintCryptoCurrenciesInformation(Dictionary<string, Data> cryptoCurrenciesInfo, Configuration configuration, RealCurrency usdToPlnInfo)
        {
            decimal usdSum = 0;
            decimal plnSum = 0;

            int i = 1;
            foreach (var cryptoCurrency in cryptoCurrenciesInfo.Values)
            {
                var currentCurrency = configuration.CryptoCurrencies.FirstOrDefault(x => x.Item1 == cryptoCurrency.Symbol);

                if (currentCurrency != null)
                    PrintLineAboutCurrentCryptoCurrency(i, currentCurrency, cryptoCurrency.Quote.Usd.Price, usdToPlnInfo.Rates.FirstOrDefault().Mid);

                usdSum += Calculation.CountCryptoValue(cryptoCurrency.Quote.Usd.Price, currentCurrency.Item2);
                plnSum += Calculation.CountCryptoValue(usdToPlnInfo.Rates.FirstOrDefault().Mid, 
                    (double)Calculation.CountCryptoValue(cryptoCurrency.Quote.Usd.Price, currentCurrency.Item2));

                i++;
            }

            PrintLineAboutCurrencySum(usdSum, plnSum);

            //TODO obsługa drukowania info z obliczaniem
            //Console.WriteLine($"Loading was successful. Loaded your {cryptoCount} crypto currencies.");
        }

        [PrinterLogAttributes(Test)]
        private static void PrintLineAboutCurrentCryptoCurrency(int iterator, Tuple<string, double> currentCurrency, double usdPrice, double plnValue)
        {
            Console.WriteLine($" {iterator} {currentCurrency.Item1}. Cena: {usdPrice}. Posiadane: {currentCurrency.Item2}. " +
                              $"Wartość: {Calculation.CountCryptoValue(usdPrice, currentCurrency.Item2)}$ " +
                              $"Wartość: {Calculation.CountCryptoValue(plnValue, (double)Calculation.CountCryptoValue(usdPrice, currentCurrency.Item2))}zł");
        }

        [PrinterLogAttributes(Test2)]
        private static void PrintLineAboutCurrencySum(decimal usdSum, decimal plnSum)
        {
            Console.WriteLine($"            Suma USD: {usdSum} | Suma PLN: {plnSum}");
        }
    }
}
