﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CryptoChecker.JsonTypes
{
    public partial class Config
    {
        [JsonProperty("supportedCurrencies")]
        public List<SupportedCurrency> SupportedCurrencies { get; set; }
    }

    public partial class SupportedCurrency
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("value")]
        public double Value { get; set; }
    }

    public partial class Config
    {
        public static Config FromJson(string json) => JsonConvert.DeserializeObject<Config>(json, Converter.Settings);
    }

    public static partial class Serialize
    {
        public static string ToJson(this Config self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    //internal static partial class Converter
    //{
    //    public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
    //    {
    //        MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
    //        DateParseHandling = DateParseHandling.None,
    //        Converters =
    //        {
    //            new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
    //        },
    //    };
    //}
}

