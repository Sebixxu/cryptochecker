﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CryptoChecker.JsonTypes
{
    public partial class CoinsId
    {
        [JsonProperty("status")]
        public Status Status { get; set; }

        [JsonProperty("data")]
        public List<Datum> Data { get; set; }
    }

    public partial class Datum
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("slug")]
        public string Slug { get; set; }

        [JsonProperty("is_active")]
        public long IsActive { get; set; }

        [JsonProperty("first_historical_data")]
        public DateTimeOffset FirstHistoricalData { get; set; }

        [JsonProperty("last_historical_data")]
        public DateTimeOffset LastHistoricalData { get; set; }

        [JsonProperty("platform")]
        public object Platform { get; set; }
    }

    public partial class CoinsId
    {
        public static CoinsId FromJson(string json) => JsonConvert.DeserializeObject<CoinsId>(json, Converter.Settings);
    }
}
