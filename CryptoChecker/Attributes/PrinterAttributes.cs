﻿using System;
using System.Collections.Generic;
using System.Text;
using ArxOne.MrAdvice.Advice;

namespace CryptoChecker.Attributes
{
    //[AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = true)]
    public class PrinterLogAttributes : Attribute, IMethodAdvice
    {
        private readonly ConsoleColor _consoleForegroundColor;
        private readonly ConsoleColor _consoleBackgroundColor;
        public PrinterLogAttributes(ConsoleColor consoleForegroundColor, ConsoleColor consoleBackgroundColor = ConsoleColor.Black)
        {
            _consoleForegroundColor = consoleForegroundColor;
            _consoleBackgroundColor = consoleBackgroundColor;
        }

        public void Advise(MethodAdviceContext context)
        {
            Console.ForegroundColor = _consoleForegroundColor;
            Console.BackgroundColor = _consoleBackgroundColor;

            try
            {
                context.Proceed();
            }
            finally
            {
                Console.ResetColor();
            }
        }
    }
}
