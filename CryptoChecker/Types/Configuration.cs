﻿using System;
using System.Collections.Generic;
using System.IO;
using CryptoChecker.JsonTypes;

namespace CryptoChecker.Types
{
    public class Configuration : IConfiguration
    {
        public List<Tuple<string, double>> CryptoCurrencies { get; set; } = new List<Tuple<string, double>>();

        public List<string> Symbols { get; set; } = new List<string>();

        public Configuration()
        {
            SetCryptoWithValues();
        }

        private Config GetConfigurationFromFile()
        {
            string fileText;

            using (StreamReader file = new StreamReader(@"D:\Projekty\Projekty C#\CryptoChecker\CryptoChecker\Config\config.json")) //TODO Relativ path
            {
                fileText = file.ReadToEnd();
            }

            return Config.FromJson(fileText);
        }

        private void SetCryptoWithValues()
        {
            var config = GetConfigurationFromFile();

            foreach (var supportedCurrency in config.SupportedCurrencies)
            {
                Symbols.Add(supportedCurrency.Symbol);
                CryptoCurrencies.Add(new Tuple<string, double>(supportedCurrency.Symbol, supportedCurrency.Value));
            }
        }
    }
}
