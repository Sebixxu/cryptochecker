﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CryptoChecker.Calculations
{
    public static class Calculation
    {
        public static decimal CountCryptoValue(double price, double value)
        {
            return (decimal)price * (decimal)value;
        }

    }
}
